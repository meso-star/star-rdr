/* Copyright (C) |Meso|Star> 2015-2018 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#ifndef SRDR_TILE_H
#define SRDR_TILE_H

#include <rsys/dynamic_array_char.h>
#include <rsys/dynamic_array_float.h>
#include <star/s3d.h>

/* Define the darray_hit data structure */
#define DARRAY_NAME hit
#define DARRAY_DATA struct s3d_hit
#include <rsys/dynamic_array.h>

struct tile {
  struct darray_char pixels; /* List of UByte RGBA pixels */
  struct darray_float dirs; /* List of float[3] ray dirs */
  struct darray_float ranges; /* List of float[2] rang ranges */
  struct darray_hit hits;
};

static FINLINE void
tile_init(struct mem_allocator* allocator, struct tile* tile)
{
  ASSERT(tile);
  darray_char_init(allocator, &tile->pixels);
  darray_float_init(allocator, &tile->dirs);
  darray_float_init(allocator, &tile->ranges);
  darray_hit_init(allocator, &tile->hits);
}

static FINLINE void
tile_release(struct tile* tile)
{
  ASSERT(tile);
  darray_char_release(&tile->pixels);
  darray_float_release(&tile->dirs);
  darray_float_release(&tile->ranges);
  darray_hit_release(&tile->hits);
}

#define CALL(Func) {                                                           \
  const res_T res = Func;                                                      \
  if(res != RES_OK) return res;                                                \
} (void)0

static FINLINE res_T
tile_copy(struct tile* dst, const struct tile* src)
{
  ASSERT(dst && src);
  CALL(darray_char_copy(&dst->pixels, &src->pixels));
  CALL(darray_float_copy(&dst->dirs, &src->dirs));
  CALL(darray_float_copy(&dst->dirs, &src->ranges));
  CALL(darray_hit_copy(&dst->hits, &src->hits));
  return RES_OK;
}

static FINLINE res_T
tile_copy_and_release(struct tile* dst, struct tile* src)
{
  ASSERT(dst && src);
  CALL(darray_char_copy_and_release(&dst->pixels, &src->pixels));
  CALL(darray_float_copy_and_release(&dst->dirs, &src->dirs));
  CALL(darray_float_copy_and_release(&dst->ranges, &src->ranges));
  CALL(darray_hit_copy_and_release(&dst->hits, &src->hits));
  return RES_OK;
}


static FINLINE res_T
tile_resize(struct tile* tile, const uint16_t width, const uint16_t height)
{
  const size_t npixels = (size_t)(width * height);
  ASSERT(tile);
  CALL(darray_char_resize(&tile->pixels, npixels * 4/*RGBA*/));
  CALL(darray_float_resize(&tile->dirs, npixels * 3/*XYZ*/));
  CALL(darray_float_resize(&tile->ranges, npixels * 2/*Near/Far*/));
  CALL(darray_hit_resize(&tile->hits, npixels));
  return RES_OK;
}
#undef CALL

static FINLINE char*
tile_get_pixels(struct tile* tile)
{
  ASSERT(tile);
  return darray_char_data_get(&tile->pixels);
}

static FINLINE float*
tile_get_directions(struct tile* tile)
{
  ASSERT(tile);
  return darray_float_data_get(&tile->dirs);
}

static FINLINE float*
tile_get_ranges(struct tile* tile)
{
  ASSERT(tile);
  return darray_float_data_get(&tile->ranges);
}

static FINLINE struct s3d_hit*
tile_get_hits(struct tile* tile)
{
  ASSERT(tile);
  return darray_hit_data_get(&tile->hits);
}

#endif /* SRDR_TILE_H */

