/* Copyright (C) |Meso|Star> 2015-2018 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#ifndef SRDR_H
#define SRDR_H

#include <rsys/rsys.h>

/* Library symbol management */
#if defined(SRDR_SHARED_BUILD) /* Build shared library */
  #define SRDR_API extern EXPORT_SYM
#elif defined(SRDR_STATIC) /* Use/build static library */
  #define SRDR_API extern LOCAL_SYM
#else /* Use shared library */
  #define SRDR_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the srdr function `Func'
 * returns an error. One should use this macro on srdr function calls for which
 * no explicit error checking is performed */
#ifndef NDEBUG
  #define SRDR(Func) ASSERT(srdr_ ## Func == RES_OK)
#else
  #define SRDR(Func) srdr_ ## Func
#endif

#define SRDR_NTHREADS_DEFAULT (~0u)

enum srdr_pixel_format { /* List of supported pixel format */
  SRDR_UBYTE_RGBA
};

static INLINE size_t
SRDR_SIZEOF_PIXEL_FORMAT(const enum srdr_pixel_format fmt)
{
  size_t BPP = 0;
  switch(fmt) {
    case SRDR_UBYTE_RGBA: BPP = sizeof(unsigned char[4]); break;
    default: FATAL("Unreachable code\n"); break;
  }
  return BPP;
}

struct srdr_camera_desc { /* Pinhole camera */
  float position[3];
  float target[3];
  float up[3];
  float proj_ratio; /* width / height */
  float fov_x; /* Horizontal field of view in radian */
};

static const struct srdr_camera_desc SRDR_CAMERA_DESC_DEFAULT = {
  { 0.f, 0.f, -500.f}, /* Position */
  { 0.f, 0.f, 0.f}, /* Target */
  { 0.f, 1.f, 0.f}, /* Up */
  1.f, /* Proj ration */
  (float)1.570796f /* ~PI/2. Horizontal field of view */
};

typedef res_T
(*srdr_pixel_write_T)
  (void* context, /* Client side defined data */
   const size_t origin[2], /* 2D coordinates of the 1st pixel to write */
   const size_t size[2], /* Number of pixels in X and Y to write */
   const enum srdr_pixel_format fmt, /* Format of the pixels to write */
   const void* pixels); /* Lost of row ordered pixels */

struct srdr_image {
  char* data; /* Row ordered pixels */
  size_t pitch; /* Size in bytes between 2 consecutive rows */
  size_t size[2]; /* Image definition */
  enum srdr_pixel_format format; /* Pixel format of the data */
};

static const struct srdr_image SRDR_IMAGE_NULL =
{ NULL, 0, {0, 0}, SRDR_UBYTE_RGBA };

enum srdr_render_type {
  SRDR_RENDER_LEGACY,
  SRDR_RENDER_NORMAL, /* Display the hit normals */
  SRDR_RENDER_FACE_ORIENTATION, /* orientation of the face normals */
  SRDR_RENDER_TYPES_COUNT__
};

/* Forward declaration of external types */
struct mem_allocator;
struct s3d_device;
struct s3d_scene;

/* Forward declaration of opaque srdr types */
struct srdr_device; /* Entry point of the library */
struct srdr_camera; /* Rendering point of view */
struct srdr_framebuffer; /* Proxy toward rendering memory */
struct srdr_render_buffer; /* Manages the image buffer memory */

BEGIN_DECLS

/*******************************************************************************
 * A device is the entry point of the srdr library. Applications
 * use a srdr_device to create others srdr resources
 ******************************************************************************/
SRDR_API res_T
srdr_device_create
  (struct mem_allocator* allocator, /* May be NULL <=> use default allocator */
   struct s3d_device* s3d, /* May be NULL <=> Internally create a s3d device */
   const unsigned nthreads_hint, /* Hint on the number of threads to use */
   struct srdr_device** dev);

SRDR_API res_T
srdr_device_ref_get
  (struct srdr_device* dev);

SRDR_API res_T
srdr_device_ref_put
  (struct srdr_device* dev);

SRDR_API res_T
srdr_device_get_s3d_device
  (struct srdr_device* dev,
   struct s3d_device** s3d);

/*******************************************************************************
 * A camera defines the rendering point of view
 ******************************************************************************/
SRDR_API res_T
srdr_camera_create
  (struct srdr_device* dev,
   struct srdr_camera** cam);

SRDR_API res_T
srdr_camera_ref_get
  (struct srdr_camera* cam);

SRDR_API res_T
srdr_camera_ref_put
  (struct srdr_camera* cam);

SRDR_API res_T
srdr_camera_setup
  (struct srdr_camera* cam,
   const struct srdr_camera_desc* desc);

SRDR_API res_T
srdr_camera_set_proj_ratio
  (struct srdr_camera* cam,
   const float proj_ratio);

SRDR_API res_T
srdr_camera_get_proj_ratio
  (const struct srdr_camera* cam,
   float* proj_ratio);

SRDR_API res_T
srdr_camera_set_fov /* Set the horizontal field of view */
  (struct srdr_camera* cam,
   const float fov); /* In radian */

SRDR_API res_T
srdr_camera_get_fov
  (const struct srdr_camera* cam,
   float* fov); /* In radian */

SRDR_API res_T
srdr_camera_look_at
  (struct srdr_camera* cam,
   const float position[3],
   const float target[3],
   const float up[3]);

SRDR_API res_T
srdr_camera_get_basis
  (struct srdr_camera* cam,
   float axis_x[3],
   float axis_y[3],
   float axis_z[3]);

SRDR_API res_T
srdr_camera_get_position
  (struct srdr_camera* cam,
   float pos[3]);

/*******************************************************************************
 * A framebuffer encapsulates the rendering memory. Note that the management of
 * the image memory space is left to the host application through the
 * srdr_pixel_write_T functor
 ******************************************************************************/
SRDR_API res_T
srdr_framebuffer_create
  (struct srdr_device* dev,
   struct srdr_framebuffer** buf);

SRDR_API res_T
srdr_framebuffer_ref_get
  (struct srdr_framebuffer* buf);

SRDR_API res_T
srdr_framebuffer_ref_put
  (struct srdr_framebuffer* buf);

SRDR_API res_T
srdr_framebuffer_setup
  (struct srdr_framebuffer* buf,
   const size_t size[2],
   const srdr_pixel_write_T writer,
   void* context); /* Host data sent as the first parameter of the writer */

/*******************************************************************************
 * A render buffer manages the image memory space
 ******************************************************************************/
SRDR_API res_T
srdr_render_buffer_create
  (struct srdr_device* dev,
   struct srdr_render_buffer** buf);

SRDR_API res_T
srdr_render_buffer_ref_get
  (struct srdr_render_buffer* buf);

SRDR_API res_T
srdr_render_buffer_ref_put
  (struct srdr_render_buffer* buf);

SRDR_API res_T
srdr_render_buffer_setup
  (struct srdr_render_buffer* buf,
   const size_t size[2],
   const enum srdr_pixel_format format);

/* Retrieve the render buffer image data. The returned image remains valid
 * until the next `srdr_render_buffer_setup' invocation */
SRDR_API res_T
srdr_render_buffer_image_get
  (struct srdr_render_buffer* buf,
   struct srdr_image* img);

/* Helper function that matches the `srdr_pixel_write_T' functor type */
SRDR_API res_T
srdr_render_buffer_write
  (void* render_buffer,
   const size_t origin[2],
   const size_t size[2],
   const enum srdr_pixel_format format,
   const void* pixels);

/*******************************************************************************
 * Miscellaneous functions
 ******************************************************************************/
SRDR_API res_T
srdr_draw
  (struct srdr_device* dev,
   const enum srdr_render_type render_type,
   struct s3d_scene* scn,
   struct srdr_camera* cam,
   struct srdr_framebuffer* buf);

END_DECLS

#endif /* SRDR_H */

