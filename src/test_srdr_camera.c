/* Copyright (C) |Meso|Star> 2015-2018 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include "test_srdr_utils.h"
#include <rsys/float3.h>

int
main(int argc, char** argv)
{
  struct srdr_camera_desc desc = SRDR_CAMERA_DESC_DEFAULT;
  struct srdr_device* dev;
  struct srdr_camera* cam;
  struct mem_allocator allocator_proxy;
  float f, x[3], y[3], z[3], pos[3], tmp0[3], tmp1[3];
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator_proxy, &mem_default_allocator);

  CHK(srdr_device_create
    (&allocator_proxy, NULL, SRDR_NTHREADS_DEFAULT, &dev) == RES_OK);

  CHK(srdr_camera_create(NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_create(dev, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_create(NULL, &cam) == RES_BAD_ARG);
  CHK(srdr_camera_create(dev, &cam) == RES_OK);

  CHK(srdr_camera_ref_get(NULL) == RES_BAD_ARG);
  CHK(srdr_camera_ref_get(cam) == RES_OK);
  CHK(srdr_camera_ref_put(NULL) == RES_BAD_ARG);
  CHK(srdr_camera_ref_put(cam) == RES_OK);
  CHK(srdr_camera_ref_put(cam) == RES_OK);

  CHK(srdr_camera_create(dev, &cam) == RES_OK);

  CHK(srdr_camera_setup(NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_setup(cam, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_setup(NULL, &desc) == RES_BAD_ARG);
  CHK(srdr_camera_setup(cam, &desc) == RES_OK);

  desc.proj_ratio = 0.f;
  CHK(srdr_camera_setup(cam, &desc) == RES_BAD_ARG);
  desc.proj_ratio = 1.f;
  CHK(srdr_camera_setup(cam, &desc) == RES_OK);
  f3_set(desc.position, desc.target);
  CHK(srdr_camera_setup(cam, &desc) == RES_BAD_ARG);
  f3_set(desc.position, SRDR_CAMERA_DESC_DEFAULT.position);
  CHK(srdr_camera_setup(cam, &desc) == RES_OK);
  f3(desc.position, 0.f, 0.f, 0.f);
  f3(desc.target, 0.f, 1.f, 0.f);
  f3(desc.up, 0.f, 1.f, 0.f);
  CHK(srdr_camera_setup(cam, &desc) == RES_BAD_ARG);
  f3(desc.up, 0.f, 0.f, 1.f);
  CHK(srdr_camera_setup(cam, &desc) == RES_OK);
  desc.fov_x = 0.f;
  CHK(srdr_camera_setup(cam, &desc) == RES_BAD_ARG);
  desc.fov_x = (float)PI;
  CHK(srdr_camera_setup(cam, &desc) == RES_BAD_ARG);
  desc.fov_x = (float)PI*0.5f;
  CHK(srdr_camera_setup(cam, &desc) == RES_OK);

  CHK(srdr_camera_set_proj_ratio(NULL, 0.f) == RES_BAD_ARG);
  CHK(srdr_camera_set_proj_ratio(cam, 0.f) == RES_BAD_ARG);
  CHK(srdr_camera_set_proj_ratio(NULL, 4.f/3.f) == RES_BAD_ARG);
  CHK(srdr_camera_set_proj_ratio(cam, 4.f/3.f) == RES_OK);
  CHK(srdr_camera_set_proj_ratio(cam, -4.f/3.f) == RES_BAD_ARG);

  CHK(srdr_camera_get_proj_ratio(NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_get_proj_ratio(cam, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_get_proj_ratio(NULL, &f) == RES_BAD_ARG);
  CHK(srdr_camera_get_proj_ratio(cam, &f) == RES_OK);
  CHK(eq_eps(f, 4.f/3.f, 1.e-6f) == 1);

  CHK(srdr_camera_set_fov(NULL, 0.f) == RES_BAD_ARG);
  CHK(srdr_camera_set_fov(cam, 0.f) == RES_BAD_ARG);
  CHK(srdr_camera_set_fov(NULL, (float)PI/4.f) == RES_BAD_ARG);
  CHK(srdr_camera_set_fov(cam, (float)PI/4.f) == RES_OK);
  CHK(srdr_camera_set_fov(cam, -(float)PI/4.f) == RES_BAD_ARG);

  CHK(srdr_camera_get_fov(NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_get_fov(cam, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_get_fov(NULL, &f) == RES_BAD_ARG);
  CHK(srdr_camera_get_fov(cam, &f) == RES_OK);
  CHK(f == (float)PI/4.f);

  CHK(srdr_camera_look_at(NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_look_at(cam, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_look_at(NULL, desc.position, NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_look_at(cam, desc.position, NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_look_at(NULL, NULL, desc.target, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_look_at(cam, NULL, desc.target, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_look_at(NULL, desc.position, desc.target, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_look_at(cam, desc.position, desc.target, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_look_at(NULL, NULL, NULL, desc.up) == RES_BAD_ARG);
  CHK(srdr_camera_look_at(cam, NULL, NULL, desc.up) == RES_BAD_ARG);
  CHK(srdr_camera_look_at(NULL, desc.position, NULL, desc.up) == RES_BAD_ARG);
  CHK(srdr_camera_look_at(cam, desc.position, NULL, desc.up) == RES_BAD_ARG);
  CHK(srdr_camera_look_at(NULL, NULL, desc.target, desc.up) == RES_BAD_ARG);
  CHK(srdr_camera_look_at(cam, NULL, desc.target, desc.up) == RES_BAD_ARG);
  CHK(srdr_camera_look_at(NULL, desc.position, desc.target, desc.up) == RES_BAD_ARG);
  CHK(srdr_camera_look_at(cam, desc.position, desc.target, desc.up) == RES_OK);

  CHK(srdr_camera_get_basis(NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_get_basis(cam, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_get_basis(NULL, x, NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_get_basis(cam, x, NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_get_basis(NULL, NULL, y, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_get_basis(cam, NULL, y, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_get_basis(NULL, x, y, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_get_basis(cam, x, y, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_get_basis(NULL, NULL, NULL, z) == RES_BAD_ARG);
  CHK(srdr_camera_get_basis(cam, NULL, NULL, z) == RES_BAD_ARG);
  CHK(srdr_camera_get_basis(NULL, x, NULL, z) == RES_BAD_ARG);
  CHK(srdr_camera_get_basis(cam, x, NULL, z) == RES_BAD_ARG);
  CHK(srdr_camera_get_basis(NULL, NULL, y, z) == RES_BAD_ARG);
  CHK(srdr_camera_get_basis(cam, NULL, y, z) == RES_BAD_ARG);
  CHK(srdr_camera_get_basis(NULL, x, y, z) == RES_BAD_ARG);
  CHK(srdr_camera_get_basis(cam, x, y, z) == RES_OK);

  CHK(srdr_camera_get_proj_ratio(cam, &desc.proj_ratio) == RES_OK);
  CHK(srdr_camera_get_fov(cam, &desc.fov_x) == RES_OK);

  f3_normalize(tmp0, f3_sub(tmp0, desc.target, desc.position));
  f3_mulf(tmp0, tmp0, (float)(1.0/tan(desc.fov_x*0.5)));
  CHK(f3_eq_eps(z, tmp0, 1.e-6f) == 1);

  f3_normalize(tmp1, f3_cross(tmp1, tmp0, desc.up));
  CHK(f3_eq_eps(x, tmp1, 1.e-6f) == 1);

  f3_normalize(tmp0, f3_cross(tmp0, tmp0, tmp1));
  f3_divf(tmp0, tmp0, desc.proj_ratio);
  CHK(f3_eq_eps(y, tmp0, 1.e-6f) == 1);

  CHK(srdr_camera_get_position(NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_get_position(cam, NULL) == RES_BAD_ARG);
  CHK(srdr_camera_get_position(NULL, pos) == RES_BAD_ARG);
  CHK(srdr_camera_get_position(cam, pos) == RES_OK);
  CHK(f3_eq_eps(pos, desc.position, 1.e-6f) == 1);

  f3(desc.position, 0.f, 0.f, 0.f);
  f3(desc.target, 0.f, 1.f, 0.f);
  f3(desc.up, 0.f, 1.f, 0.f);
  CHK(srdr_camera_look_at(cam, desc.position, desc.target, desc.up) == RES_BAD_ARG);
  f3(desc.up, 0.f, 0.f, 1.f);
  CHK(srdr_camera_look_at(cam, desc.position, desc.target, desc.up) == RES_OK);
  f3(desc.target, 0.f, 0.f, 0.f);
  CHK(srdr_camera_look_at(cam, desc.position, desc.target, desc.up) == RES_BAD_ARG);

  CHK(srdr_device_ref_put(dev) == RES_OK);
  CHK(srdr_camera_ref_put(cam) == RES_OK);

  check_memory_leaks(&allocator_proxy);
  mem_shutdown_proxy_allocator(&allocator_proxy);
  CHK(mem_allocated_size() == 0);
  return 0;
}

