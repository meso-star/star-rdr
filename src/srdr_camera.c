/* Copyright (C) |Meso|Star> 2015-2018 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include "srdr.h"
#include "srdr_camera.h"
#include "srdr_device.h"

#include <rsys/mem_allocator.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE res_T
axis_compute
  (const float position[3],
   const float target[3],
   const float up[3],
   const float fov_x,
   const float rcp_proj_ratio,
   float axis_x[3],
   float axis_y[3],
   float axis_z[3])
{
  float img_plane_depth;
  ASSERT(position && target && up && fov_x > 0.f && rcp_proj_ratio > 0.f);
  ASSERT(axis_x && axis_y && axis_z);

  if(f3_normalize(axis_z, f3_sub(axis_z, target, position)) <= 0.f)
    return RES_BAD_ARG;
  if(f3_normalize(axis_x, f3_cross(axis_x, axis_z, up)) <= 0.f)
    return RES_BAD_ARG;
  if(f3_normalize(axis_y, f3_cross(axis_y, axis_z, axis_x)) <= 0.f)
    return RES_BAD_ARG;

  img_plane_depth = (float)(1.0/tan(fov_x*0.5));
  f3_mulf(axis_z, axis_z, img_plane_depth);
  f3_mulf(axis_y, axis_y, rcp_proj_ratio);
  return RES_OK;
}

static void
camera_release(ref_T* ref)
{
  struct srdr_camera* cam = CONTAINER_OF(ref, struct srdr_camera, ref);
  struct srdr_device* dev;
  ASSERT(ref);
  dev = cam->dev;
  MEM_RM(dev->allocator, cam);
  SRDR(device_ref_put(dev));
}


/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
srdr_camera_create(struct srdr_device* dev, struct srdr_camera** cam_out)
{
  struct srdr_camera* cam = NULL;
  res_T res = RES_OK;

  if(!dev || !cam_out) {
    res = RES_BAD_ARG;
    goto error;
  }
  cam = MEM_CALLOC(dev->allocator, 1, sizeof(struct srdr_camera));
  if(!cam) {
    res = RES_MEM_ERR;
    goto error;
  }
  SRDR(device_ref_get(dev));
  cam->dev = dev;
  ref_init(&cam->ref);
  SRDR(camera_setup(cam, &SRDR_CAMERA_DESC_DEFAULT));
exit:
  if(cam_out)
    *cam_out = cam;
  return res;
error:
  if(cam) {
    SRDR(camera_ref_put(cam));
    cam = NULL;
  }
  goto exit;
}

res_T
srdr_camera_ref_get(struct srdr_camera* cam)
{
  if(!cam) return RES_BAD_ARG;
  ref_get(&cam->ref);
  return RES_OK;
}

res_T
srdr_camera_ref_put(struct srdr_camera* cam)
{
  if(!cam) return RES_BAD_ARG;
  ref_put(&cam->ref, camera_release);
  return RES_OK;
}

res_T
srdr_camera_setup(struct srdr_camera* cam, const struct srdr_camera_desc* desc)
{
  float axis_x[3], axis_y[3], axis_z[3];
  float rcp_proj_ratio;
  res_T res;

  if(!cam || !desc || desc->fov_x <= 0.f
  || desc->fov_x > (float)MDEG2RAD(120.0) || desc->proj_ratio <= 0.f)
    return RES_BAD_ARG;

  rcp_proj_ratio = 1.f / desc->proj_ratio;
  res = axis_compute
    (desc->position, desc->target, desc->up, desc->fov_x, rcp_proj_ratio,
     axis_x, axis_y, axis_z);
  if(res != RES_OK)
    return res;
  f3_set(cam->axis_x, axis_x);
  f3_set(cam->axis_y, axis_y);
  f3_set(cam->axis_z, axis_z);
  f3_set(cam->position, desc->position);
  cam->fov_x = desc->fov_x;
  cam->rcp_proj_ratio = rcp_proj_ratio;
  return RES_OK;
}

res_T
srdr_camera_set_proj_ratio(struct srdr_camera* cam, const float proj_ratio)
{
  float axis_y[3] = {0.f, 0.f, 0.f};
  if(!cam || proj_ratio <= 0.f)
    return RES_BAD_ARG;
  if(f3_normalize(axis_y, cam->axis_y) <= 0.f)
    return RES_BAD_ARG;
  cam->rcp_proj_ratio = 1.f / proj_ratio;
  f3_mulf(cam->axis_y, axis_y, cam->rcp_proj_ratio);
  return RES_OK;
}

res_T
srdr_camera_get_proj_ratio(const struct srdr_camera* cam, float* proj_ratio)
{
  if(!cam || !proj_ratio)
    return RES_BAD_ARG;
  *proj_ratio = 1.f / cam->rcp_proj_ratio;
  return RES_OK;
}

res_T
srdr_camera_set_fov(struct srdr_camera* cam, const float fov_x)
{
  float img_plane_depth;
  if(!cam || fov_x <= 0.f)
    return RES_BAD_ARG;
  img_plane_depth = (float)(1.0/tan(fov_x*0.5f));
  f3_normalize(cam->axis_z, cam->axis_z);
  f3_mulf(cam->axis_z, cam->axis_z, img_plane_depth);
  cam->fov_x = fov_x;
  return RES_OK;
}

res_T
srdr_camera_get_fov(const struct srdr_camera* cam, float* fov_x)
{
  if(!cam || !fov_x) return RES_BAD_ARG;
  *fov_x = cam->fov_x;
  return RES_OK;
}

res_T
srdr_camera_look_at
  (struct srdr_camera* cam,
   const float pos[3],
   const float target[3],
   const float up[3])
{
  float axis_x[3], axis_y[3], axis_z[3];
  res_T res;

  if(!cam || !pos || !target || !up)
    return RES_BAD_ARG;

  res = axis_compute
    (pos, target, up, cam->fov_x, cam->rcp_proj_ratio, axis_x, axis_y, axis_z);
  if(res != RES_OK)
    return res;
  f3_set(cam->axis_x, axis_x);
  f3_set(cam->axis_y, axis_y);
  f3_set(cam->axis_z, axis_z);
  f3_set(cam->position, pos);
  return RES_OK;
}

res_T
srdr_camera_get_basis
  (struct srdr_camera* cam,
   float axis_x[3],
   float axis_y[3],
   float axis_z[3])
{
  if(!cam || !axis_x || !axis_y || !axis_z)
    return RES_BAD_ARG;
  f3_set(axis_x, cam->axis_x);
  f3_set(axis_y, cam->axis_y);
  f3_set(axis_z, cam->axis_z);
  return RES_OK;
}

res_T
srdr_camera_get_position(struct srdr_camera* cam, float pos[3])
{
  if(!cam || !pos) return RES_BAD_ARG;
  f3_set(pos, cam->position);
  return RES_OK;
}

