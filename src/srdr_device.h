/* Copyright (C) |Meso|Star> 2015-2018 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#ifndef SRDR_DEVICE_H
#define SRDR_DEVICE_H

#include "srdr_tile.h"

#include <rsys/dynamic_array.h>
#include <rsys/ref_count.h>

/* Declare the darray_tile data structure */
#define DARRAY_NAME tile
#define DARRAY_DATA struct tile
#define DARRAY_FUNCTOR_INIT tile_init
#define DARRAY_FUNCTOR_RELEASE tile_release
#define DARRAY_FUNCTOR_COPY tile_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE tile_copy_and_release
#include <rsys/dynamic_array.h>

struct srdr_device {
  struct s3d_device* s3d;

  ref_T ref;
  struct mem_allocator* allocator;
  struct darray_tile tiles; /* Per thread local rendering memory */
};

#endif /* SRDR_DEVICE_H */

