/* Copyright (C) |Meso|Star> 2015-2018 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include "srdr.h"
#include "srdr_device.h"
#include "srdr_tile.h"

#include <star/s3d.h>
#include <rsys/mem_allocator.h>

#include <omp.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
device_release(ref_T* ref)
{
  struct srdr_device* dev = CONTAINER_OF(ref, struct srdr_device, ref);
  ASSERT(ref);
  if(dev->s3d)
    S3D(device_ref_put(dev->s3d));
  darray_tile_release(&dev->tiles);
  MEM_RM(dev->allocator, dev);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
srdr_device_create
  (struct mem_allocator* allocator,
   struct s3d_device* s3d,
   const unsigned nthreads_hint,
   struct srdr_device** dev_out)
{
  struct srdr_device* dev = NULL;
  struct mem_allocator* mem_allocator;
  unsigned nthreads;
  res_T res = RES_OK;

  if(!dev_out || !nthreads_hint) {
    res = RES_BAD_ARG;
    goto error;
  }

  mem_allocator = allocator ? allocator : &mem_default_allocator;
  dev = MEM_CALLOC(mem_allocator, 1, sizeof(struct srdr_device));
  if(!dev) {
    res = RES_MEM_ERR;
    goto error;
  }
  dev->allocator = mem_allocator;
  ref_init(&dev->ref);
  darray_tile_init(dev->allocator, &dev->tiles);
  if(!s3d) {
    res = s3d_device_create(NULL, dev->allocator, 0, &s3d);
  } else {
    res = s3d_device_ref_get(s3d);
  }
  if(res != RES_OK)
    goto error;
  dev->s3d = s3d;

  nthreads = (unsigned)omp_get_num_procs();
  if(nthreads > nthreads_hint)
    nthreads = nthreads_hint;
  omp_set_num_threads((int)nthreads);
  res = darray_tile_resize(&dev->tiles, nthreads);
  if(res != RES_OK)
    goto error;

exit:
  if(dev_out)
    *dev_out = dev;
  return res;
error:
  if(dev) {
    SRDR(device_ref_put(dev));
    dev = NULL;
  }
  goto exit;
}

res_T
srdr_device_ref_get(struct srdr_device* dev)
{
  if(!dev) return RES_BAD_ARG;
  ref_get(&dev->ref);
  return RES_OK;
}

res_T
srdr_device_ref_put(struct srdr_device* dev)
{
  if(!dev) return RES_BAD_ARG;
  ref_put(&dev->ref, device_release);
  return RES_OK;
}

res_T
srdr_device_get_s3d_device(struct srdr_device* dev, struct s3d_device** s3d)
{
  if(!dev || !s3d) return RES_BAD_ARG;
  *s3d = dev->s3d;
  return RES_OK;
}

