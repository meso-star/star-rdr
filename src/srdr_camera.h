/* Copyright (C) |Meso|Star> 2015-2018 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#ifndef SRDR_CAMERA_H
#define SRDR_CAMERA_H

#include <rsys/float3.h>
#include <rsys/ref_count.h>

struct srdr_camera {
  float axis_x[3], axis_y[3], axis_z[3];  /* Camera orthogonal basis */
  float position[3];
  float fov_x; /* Field of view in radians */
  float rcp_proj_ratio; /* height / width */

  ref_T ref;
  struct srdr_device* dev;
};

static FINLINE void
camera_ray
  (const struct srdr_camera* cam,
   const float sample[2],
   float org[3],
   float dir[3])
{
  float x[3], y[3], len;
  (void)len; /* avoid warning in debug */
  ASSERT(cam && sample && org && dir);

  f3_mulf(x, cam->axis_x, sample[0]*2.f - 1.f);
  f3_mulf(y, cam->axis_y, sample[1]*2.f - 1.f);
  f3_add(dir, f3_add(dir, x, y), cam->axis_z);
  len = f3_normalize(dir, dir);
  ASSERT(len >= 1.e-6f);
  f3_set(org, cam->position);
}

#endif /* SRDR_CAMERA_H */

