/* Copyright (C) |Meso|Star> 2015-2018 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include "test_srdr_utils.h"

#include <rsys/float3.h>
#include <rsys/image.h>

#include <star/s3d.h>

struct mesh {
  const float* verts;
  size_t nverts;
  const unsigned* ids;
  size_t nids;
};

static uint64_t
get_ntris(void* data)
{
  CHK(data != NULL);
  return ((struct mesh*)data)->nids / 3;
}

static void
get_ids(const unsigned itri, unsigned ids[3], void* data)
{
  const unsigned id = itri * 3;
  struct mesh* msh = data;
  CHK(itri < get_ntris(data));
  CHK(id + 2 < msh->nids);
  ids[0] = msh->ids[id + 0];
  ids[1] = msh->ids[id + 1];
  ids[2] = msh->ids[id + 2];
}

static void
get_pos(const unsigned ivert, float verts[3], void* data)
{
  struct mesh* msh = data;
  CHK(ivert < msh->nverts);
  f3_set(verts, msh->verts + (ivert*3));
}

static void
scene_create(struct s3d_device* dev, struct s3d_scene** scn)
{
  const float verts[] = {
    /* Box */
    552.f, 0.f,   0.f,
    0.f,   0.f,   0.f,
    0.f,   559.f, 0.f,
    552.f, 559.f, 0.f,
    552.f, 0.f,   548.f,
    0.f,   0.f,   548.f,
    0.f,   559.f, 548.f,
    552.f, 559.f, 548.f,
    /* Short block */
    130.f, 65.f,  0.f,
    82.f,  225.f, 0.f,
    240.f, 272.f, 0.f,
    290.f, 114.f, 0.f,
    130.f, 65.f,  165.f,
    82.f,  225.f, 165.f,
    240.f, 272.f, 165.f,
    290.f, 114.f, 165.f,
    /* Tall block */
    423.0f, 247.0f, 0.f,
    265.0f, 296.0f, 0.f,
    314.0f, 456.0f, 0.f,
    472.0f, 406.0f, 0.f,
    423.0f, 247.0f, 330.f,
    265.0f, 296.0f, 330.f,
    314.0f, 456.0f, 330.f,
    472.0f, 406.0f, 330.f
  };
  const unsigned nverts = (unsigned)(sizeof(verts) / (sizeof(float)*3));
  const unsigned ids[] = {
    /* Box */
    0, 1, 2, 2, 3, 0,
    4, 5, 6, 6, 7, 4,
    1, 2, 6, 6, 5, 1,
    0, 3, 7, 7, 4, 0,
    2, 3, 7, 7, 6, 2,
    /* Short block */
    12, 13, 14, 14, 15, 12,
    9, 10, 14, 14, 13, 9,
    8, 11, 15, 15, 12, 8,
    10, 11, 15, 15, 14, 10,
    8, 9, 13, 13, 12, 8,
    /* Tall block */
    20, 21, 22, 22, 23, 20,
    17, 18, 22, 22, 21, 17,
    16, 19, 23, 23, 20, 16,
    18, 19, 23, 23, 22, 18,
    16, 17, 21, 21, 20, 16
  };
  const unsigned nids = (unsigned)(sizeof(ids)/sizeof(uint32_t));
  struct s3d_shape* shape;
  struct mesh msh;
  struct s3d_vertex_data attribs;

  attribs.usage = S3D_POSITION;
  attribs.type = S3D_FLOAT3;
  attribs.get = get_pos;

  msh.verts = verts;
  msh.nverts = nverts;
  msh.ids = ids;
  msh.nids = nids;

  CHK(scn != NULL);
  CHK(s3d_scene_create(dev, scn) == RES_OK);
  CHK(s3d_shape_create_mesh(dev, &shape) == RES_OK);
  CHK(s3d_mesh_setup_indexed_vertices
    (shape, (unsigned)(nids/3), get_ids, nverts, &attribs, 1, &msh) == RES_OK);

  CHK(s3d_scene_attach_shape(*scn, shape) == RES_OK);
  CHK(s3d_shape_ref_put(shape) == RES_OK);
}

int
main(int argc, char** argv)
{
  struct srdr_device* dev;
  struct srdr_camera* cam;
  struct srdr_framebuffer* fbuf;
  struct srdr_render_buffer* rbuf;
  struct s3d_device* s3d;
  struct s3d_scene* scn;
  struct srdr_camera_desc cam_desc = SRDR_CAMERA_DESC_DEFAULT;
  struct srdr_image img;
  struct image img_ppm;
  struct mem_allocator allocator_proxy;
  const size_t img_size[2] = { 800, 600 };
  size_t x, y;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator_proxy, &mem_default_allocator);

  CHK(s3d_device_create(NULL, &allocator_proxy, 0, &s3d) == RES_OK);
  scene_create(s3d, &scn);

  CHK(srdr_device_create
    (&allocator_proxy, s3d, SRDR_NTHREADS_DEFAULT, &dev) == RES_OK);

  CHK(srdr_camera_create(dev, &cam) == RES_OK);
  f3(cam_desc.position, 278.f, -1000.f, 273.f);
  f3(cam_desc.target, 278.f, 0.f, 273.f);
  f3(cam_desc.up, 0.f, 0.f, 1.f);
  cam_desc.proj_ratio  = (float)img_size[0]/(float)img_size[1];
  cam_desc.fov_x = (float)PI*0.25f;
  CHK(srdr_camera_setup(cam, &cam_desc) == RES_OK);

  CHK(srdr_render_buffer_create(dev, &rbuf) == RES_OK);
  CHK(srdr_render_buffer_setup(rbuf, img_size, SRDR_UBYTE_RGBA) == RES_OK);
  CHK(srdr_framebuffer_create(dev, &fbuf) == RES_OK);
  CHK(srdr_framebuffer_setup
    (fbuf, img_size, srdr_render_buffer_write, rbuf) == RES_OK);

  CHK(srdr_draw(NULL, SRDR_RENDER_LEGACY, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_draw(dev, SRDR_RENDER_LEGACY, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_draw(NULL, SRDR_RENDER_LEGACY, scn, NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_draw(dev, SRDR_RENDER_LEGACY, scn, NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_draw(NULL, SRDR_RENDER_LEGACY, NULL, cam, NULL) == RES_BAD_ARG);
  CHK(srdr_draw(dev, SRDR_RENDER_LEGACY, NULL, cam, NULL) == RES_BAD_ARG);
  CHK(srdr_draw(NULL, SRDR_RENDER_LEGACY, scn, cam, NULL) == RES_BAD_ARG);
  CHK(srdr_draw(dev, SRDR_RENDER_LEGACY, scn, cam, NULL) == RES_BAD_ARG);
  CHK(srdr_draw(NULL, SRDR_RENDER_LEGACY, NULL, NULL, fbuf) == RES_BAD_ARG);
  CHK(srdr_draw(dev, SRDR_RENDER_LEGACY, NULL, NULL, fbuf) == RES_BAD_ARG);
  CHK(srdr_draw(NULL, SRDR_RENDER_LEGACY, scn, NULL, fbuf) == RES_BAD_ARG);
  CHK(srdr_draw(dev, SRDR_RENDER_LEGACY, scn, NULL, fbuf) == RES_BAD_ARG);
  CHK(srdr_draw(NULL, SRDR_RENDER_LEGACY, NULL, cam, fbuf) == RES_BAD_ARG);
  CHK(srdr_draw(dev, SRDR_RENDER_LEGACY, NULL, cam, fbuf) == RES_BAD_ARG);
  CHK(srdr_draw(NULL, SRDR_RENDER_LEGACY, scn, cam, fbuf) == RES_BAD_ARG);
  CHK(srdr_draw(dev, SRDR_RENDER_LEGACY, scn, cam, fbuf) == RES_OK);

  CHK(srdr_render_buffer_image_get(rbuf, &img) == RES_OK);
  CHK(img.size[0] == img_size[0]);
  CHK(img.size[1] == img_size[1]);
  CHK(img.format == SRDR_UBYTE_RGBA);
  CHK(img.pitch == img_size[0] * SRDR_SIZEOF_PIXEL_FORMAT(img.format));

  CHK(image_init(&allocator_proxy, &img_ppm) == RES_OK);
  CHK(image_setup
    (&img_ppm, img.size[0], img.size[1], img.pitch, IMAGE_RGB8, NULL) == RES_OK);
  FOR_EACH(y, 0, img.size[1]) {
  FOR_EACH(x, 0, img.size[0]) {
    const char* src = img.data + y*img.pitch + x*sizeof(uint8_t[4]);
    char* dst = img_ppm.pixels + y*img.pitch + x*sizeof(uint8_t[3]);
    dst[0] = src[0];
    dst[1] = src[1];
    dst[2] = src[2];
  }}
  CHK(image_write_ppm_stream(&img_ppm, 0, stdout) == RES_OK);
  CHK(image_release(&img_ppm) == RES_OK);

  CHK(srdr_draw(dev, SRDR_RENDER_NORMAL, scn, cam, fbuf) == RES_OK);
  CHK(srdr_draw
    (dev, SRDR_RENDER_FACE_ORIENTATION, scn, cam, fbuf) == RES_OK);
  CHK(srdr_draw(dev, SRDR_RENDER_TYPES_COUNT__, scn, cam, fbuf) == RES_BAD_ARG);

  CHK(srdr_device_ref_put(dev) == RES_OK);
  CHK(srdr_camera_ref_put(cam) == RES_OK);
  CHK(srdr_framebuffer_ref_put(fbuf) == RES_OK);
  CHK(srdr_render_buffer_ref_put(rbuf) == RES_OK);
  CHK(s3d_device_ref_put(s3d) == RES_OK);
  CHK(s3d_scene_ref_put(scn) == RES_OK);

  check_memory_leaks(&allocator_proxy);
  mem_shutdown_proxy_allocator(&allocator_proxy);
  CHK(mem_allocated_size() == 0);
  return 0;
}

