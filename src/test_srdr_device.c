/* Copyright (C) |Meso|Star> 2015-2018 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include "test_srdr_utils.h"
#include <star/s3d.h>

int
main(int argc, char** argv)
{
  struct srdr_device* dev;
  struct s3d_device* s3d = NULL, *s3d2 = NULL;
  struct mem_allocator allocator_proxy;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator_proxy, &mem_default_allocator);

  CHK(srdr_device_create(NULL, NULL, SRDR_NTHREADS_DEFAULT, NULL) == RES_BAD_ARG);
  CHK(srdr_device_create
    (&allocator_proxy, NULL, SRDR_NTHREADS_DEFAULT, NULL) == RES_BAD_ARG);
  CHK(srdr_device_create
    (NULL, NULL, SRDR_NTHREADS_DEFAULT, &dev) == RES_OK);

  CHK(srdr_device_get_s3d_device(NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_device_get_s3d_device(dev, NULL) == RES_BAD_ARG);
  CHK(srdr_device_get_s3d_device(NULL, &s3d) == RES_BAD_ARG);
  CHK(srdr_device_get_s3d_device(dev, &s3d) == RES_OK);
  CHK(s3d2 != s3d);

  CHK(srdr_device_ref_get(NULL) == RES_BAD_ARG);
  CHK(srdr_device_ref_get(dev) == RES_OK);
  CHK(srdr_device_ref_put(NULL) == RES_BAD_ARG);
  CHK(srdr_device_ref_put(dev) == RES_OK);
  CHK(srdr_device_ref_put(dev) == RES_OK);

  CHK(srdr_device_create(NULL, NULL, 0, &dev) == RES_BAD_ARG);
  CHK(srdr_device_create
    (&allocator_proxy, NULL, SRDR_NTHREADS_DEFAULT, &dev) == RES_OK);
  CHK(srdr_device_ref_put(dev) == RES_OK);
  CHK(s3d_device_create(NULL, &allocator_proxy, 0, &s3d) == RES_OK);
  CHK(srdr_device_create(NULL, s3d, SRDR_NTHREADS_DEFAULT, &dev) == RES_OK);
  CHK(srdr_device_ref_put(dev) == RES_OK);
  CHK(srdr_device_create
    (&allocator_proxy, s3d, SRDR_NTHREADS_DEFAULT, &dev) == RES_OK);
  CHK(srdr_device_get_s3d_device(dev, &s3d2) == RES_OK);
  CHK(s3d2 == s3d);

  CHK(srdr_device_ref_put(dev) == RES_OK);
  CHK(s3d_device_ref_put(s3d) == RES_OK);

  check_memory_leaks(&allocator_proxy);
  mem_shutdown_proxy_allocator(&allocator_proxy);
  CHK(mem_allocated_size() == 0);
  return 0;
}

