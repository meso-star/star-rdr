/* Copyright (C) |Meso|Star> 2015-2018 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include "test_srdr_utils.h"

int
main(int argc, char** argv)
{
  struct srdr_device* dev;
  struct srdr_framebuffer* buf;
  struct mem_allocator allocator_proxy;
  const size_t size[2] = { 320, 240 };
  struct srdr_render_buffer* rbuf;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator_proxy, &mem_default_allocator);

  CHK(srdr_device_create
    (&allocator_proxy, NULL, SRDR_NTHREADS_DEFAULT, &dev) == RES_OK);

  CHK(srdr_framebuffer_create(NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_framebuffer_create(dev, NULL) == RES_BAD_ARG);
  CHK(srdr_framebuffer_create(NULL, &buf) == RES_BAD_ARG);
  CHK(srdr_framebuffer_create(dev, &buf) == RES_OK);

  CHK(srdr_framebuffer_ref_get(NULL) == RES_BAD_ARG);
  CHK(srdr_framebuffer_ref_get(buf) == RES_OK);
  CHK(srdr_framebuffer_ref_put(NULL) == RES_BAD_ARG);
  CHK(srdr_framebuffer_ref_put(buf) == RES_OK);
  CHK(srdr_framebuffer_ref_put(buf) == RES_OK);

  CHK(srdr_framebuffer_create(dev, &buf) == RES_OK);

  CHK(srdr_framebuffer_setup(NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_framebuffer_setup(buf, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_framebuffer_setup(NULL, size, NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_framebuffer_setup(buf, size, NULL, NULL) == RES_BAD_ARG);
  CHK(srdr_framebuffer_setup(NULL, NULL, srdr_render_buffer_write, NULL) == RES_BAD_ARG);
  CHK(srdr_framebuffer_setup(buf, NULL, srdr_render_buffer_write, NULL) == RES_BAD_ARG);
  CHK(srdr_framebuffer_setup(NULL, size, srdr_render_buffer_write, NULL) == RES_BAD_ARG);
  CHK(srdr_framebuffer_setup(buf, size, srdr_render_buffer_write, NULL) == RES_OK);

  CHK(srdr_render_buffer_create(dev, &rbuf) == RES_OK);
  CHK(srdr_render_buffer_setup(rbuf, size, SRDR_UBYTE_RGBA) == RES_OK);
  CHK(srdr_framebuffer_setup(buf, size, srdr_render_buffer_write, rbuf) == RES_OK);

  CHK(srdr_render_buffer_ref_put(rbuf) == RES_OK);
  CHK(srdr_framebuffer_ref_put(buf) == RES_OK);
  CHK(srdr_device_ref_put(dev) == RES_OK);

  check_memory_leaks(&allocator_proxy);
  mem_shutdown_proxy_allocator(&allocator_proxy);
  CHK(mem_allocated_size() == 0);
  return 0;
}

