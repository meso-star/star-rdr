/* Copyright (C) |Meso|Star> 2015-2018 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include "srdr.h"
#include "srdr_camera.h"
#include "srdr_device.h"
#include "srdr_framebuffer.h"

#include <rsys/float2.h>
#include <rsys/math.h>

#include <omp.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static FINLINE uint16_t
morton2D_decode(const uint32_t u32)
{
  uint32_t x = u32 & 0x55555555;
  x = (x | (x >> 1)) & 0x33333333;
  x = (x | (x >> 2)) & 0x0F0F0F0F;
  x = (x | (x >> 4)) & 0x00FF00FF;
  x = (x | (x >> 8)) & 0x0000FFFF;
  return (uint16_t)x;
}

static FINLINE uint32_t
morton2D_encode(const uint16_t u16)
{
  uint32_t x = u16;
  x = (x | (x << 8)) & 0x00FF00FF;
  x = (x | (x << 4)) & 0x0F0F0F0F;
  x = (x | (x << 2)) & 0x33333333;
  x = (x | (x << 1)) & 0x55555555;
  return x;
}

static FINLINE void
hit_shade
  (const struct s3d_hit* hit,
   const enum srdr_render_type render_type,
   const float eye_pos[3],
   const float ray_dir[3],
   unsigned char rgba[4])
{
  int i;
  ASSERT(hit && ray_dir && rgba);

  if(S3D_HIT_NONE(hit)) {
    FOR_EACH(i, 0, 3) rgba[i] = 0;
    rgba[3] = 255;
  } else {
    float wi[3], cosine;
    float N[3] = {0.f, 0.f, 0.f};
    float color[3];
    struct s3d_attrib attr;

    f3_normalize(N, hit->normal);
    switch(render_type) {
      case SRDR_RENDER_LEGACY:
        f3_normalize(N, hit->normal);
        if(0 > (cosine = f3_dot(N, ray_dir)))
          cosine = f3_dot(N, f3_minus(wi, ray_dir));
        FOR_EACH(i, 0, 3)
          rgba[i] = (unsigned char)(255.f*cosine);
        rgba[3] = 255;
        break;
      case SRDR_RENDER_NORMAL:
        f3_normalize(N, hit->normal);
        FOR_EACH(i, 0, 3) rgba[i] = (unsigned char)(absf(N[i])*255.f);
        rgba[3] = 255;
        break;
      case SRDR_RENDER_FACE_ORIENTATION:
        S3D(primitive_get_attrib(&hit->prim, S3D_POSITION, hit->uv, &attr));
        if(f3_dot(N, eye_pos) - f3_dot(N, attr.value) >= 0.f) {
          f3(color, 0.f, 0.f, 1.f); /* Front face */
        } else {
          f3(color, 1.f, 0.f, 0.f); /* Back face */
        }
        if(0 > (cosine = f3_dot(N, ray_dir)))
          cosine = f3_dot(N, f3_minus(wi, ray_dir));

        FOR_EACH(i, 0, 3)
          rgba[i] = (unsigned char)(255.f*cosine*color[i]);
        rgba[3] = 255;
        break;
      default: FATAL("Unreachable code\n");
    }
  }
}

static res_T
ray_trace_tile
  (struct tile* tile,
   const enum srdr_render_type render_type,
   struct s3d_scene_view* view,
   struct srdr_camera* cam,
   struct srdr_framebuffer* fbuf,
   const size_t tile_origin[2],
   const size_t tile_size[2])
{
  float* ray_dirs, *ray_ranges;
  struct s3d_hit* hits;
  uint32_t npixels;
  uint32_t mcode; /* Morton code */
  uint16_t ipixel_x, ipixel_y;
  float ray_org[3];
  float pixel_size[2];
  unsigned char* pixels = NULL;
  res_T res = RES_OK;
  ASSERT(tile && view && cam && fbuf && tile_origin && tile_size);

  pixel_size[0] = 1.f / (float)fbuf->size[0];
  pixel_size[1] = 1.f / (float)fbuf->size[1];

  /* Adjust the # pixels to process them with respect to a morton order */
  npixels = (uint32_t)round_up_pow2(MMAX(tile_size[0], tile_size[1]));
  npixels *= npixels;

  /* Retrieve the tile data */
  pixels = (unsigned char*)tile_get_pixels(tile);
  ray_dirs = tile_get_directions(tile);
  ray_ranges = tile_get_ranges(tile);
  hits = tile_get_hits(tile);

  /* Camera ray generation */
  FOR_EACH(mcode, 0, npixels) {
    size_t idir = mcode*3;
    size_t irange = mcode*2;
    float sample[2];

    /* Compute the pixel indices in tile space */
    ipixel_x = morton2D_decode(mcode>>0);
    if(ipixel_x >= tile_size[0]) {
      f2(ray_ranges + irange, FLT_MAX, -FLT_MAX); /* disable the ray */
      continue;
    }
    ipixel_y = morton2D_decode(mcode>>1);
    if(ipixel_y >= tile_size[1]) {
      f2(ray_ranges + irange, FLT_MAX, -FLT_MAX); /* disable the ray */
      continue;
    }
    f2(ray_ranges + irange, 0, FLT_MAX);
    /* Transform pixel indices from tile space to image space */
    ipixel_x = (uint16_t)(ipixel_x + tile_origin[0]);
    ipixel_y = (uint16_t)(ipixel_y + tile_origin[1]);
    /* Sample the pixel center */
    sample[0] = ((float)ipixel_x + 0.5f) * pixel_size[0];
    sample[1] = ((float)ipixel_y + 0.5f) * pixel_size[1];
    camera_ray(cam, sample, ray_org, ray_dirs + idir);
  }

  /* Trace the rays through the image plane sample */
  res = s3d_scene_view_trace_rays
    (view, npixels, S3D_RAYS_SINGLE_ORIGIN|S3D_RAYS_SINGLE_RANGE,
     ray_org, ray_dirs, ray_ranges, NULL, 0, hits);
  if(res != RES_OK) return res;

  /* Shading */
  FOR_EACH(mcode, 0, npixels) {
    unsigned char* rgba;
    struct s3d_hit* hit = hits + mcode;
    /* Compute the pixel indices in tile space */
    ipixel_x = morton2D_decode(mcode>>0);
    if(ipixel_x >= tile_size[0]) continue;
    ipixel_y = morton2D_decode(mcode>>1);
    if(ipixel_y >= tile_size[1]) continue;
    /* Retrieve local pixel memory */
    rgba = pixels + (ipixel_y * tile_size[0] + ipixel_x) * 4 /* #channels */;
    /* Shading */
    hit_shade(hit, render_type, cam->position, ray_dirs + mcode * 3, rgba);
  }
  return RES_OK;
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
srdr_draw
  (struct srdr_device* dev,
   const enum srdr_render_type render_type,
   struct s3d_scene* scn,
   struct srdr_camera* cam,
   struct srdr_framebuffer* buf)
{
  struct tile* tiles = NULL;
  struct s3d_scene_view* view = NULL;
  const uint16_t tile_width = 32;
  const uint16_t tile_height = 32;
  uint16_t ntiles_x, ntiles_y;
  uint32_t ntiles;
  int64_t mcode; /* Morton code of a tile */
  ATOMIC res = RES_OK;

  if(!dev || !scn || !cam || !buf) {
    res = RES_BAD_ARG;
    goto error;
  }

  if((unsigned)render_type >= SRDR_RENDER_TYPES_COUNT__) {
    res = RES_BAD_ARG;
    goto error;
  }

  res = s3d_scene_view_create(scn, S3D_TRACE, &view);
  if(res != RES_OK) goto error;

  ntiles_x = (uint16_t)((buf->size[0] + (tile_width-1u)/*ceil*/)/tile_width);
  ntiles_y = (uint16_t)((buf->size[1] + (tile_height-1u)/*ceil*/)/tile_height);
  /* Adjust the # tiles to process them with respect to a morton order */
  ntiles = (uint32_t)round_up_pow2(MMAX(ntiles_x, ntiles_y));
  ntiles *= ntiles;

  tiles = darray_tile_data_get(&dev->tiles);

  #pragma omp parallel for schedule(dynamic)
  for(mcode = 0; mcode < (int64_t)ntiles; ++mcode) {
    const int itile = omp_get_thread_num();
    res_T res_local = RES_OK;

    if(ATOMIC_GET(&res) != RES_OK) continue;

    res_local = tile_resize(&tiles[itile], tile_width, tile_height);
    if(res_local != RES_OK) {
      ATOMIC_SET(&res, res_local);
    } else {
      size_t tile_origin[2];
      size_t tile_size[2];
      ASSERT((size_t)itile < darray_tile_size_get(&dev->tiles));

      /* Origin of the tile to draw */
      tile_origin[0] = morton2D_decode((uint32_t)mcode>>0);
      if(tile_origin[0] >= ntiles_x) continue;
      tile_origin[1] = morton2D_decode((uint32_t)mcode>>1);
      if(tile_origin[1] >= ntiles_y) continue;
      tile_origin[0] = tile_origin[0] * tile_width;
      tile_origin[1] = tile_origin[1] * tile_height;
      tile_size[0] = (uint16_t)MMIN(tile_width, buf->size[0]-tile_origin[0]);
      tile_size[1] = (uint16_t)MMIN(tile_height, buf->size[1]-tile_origin[1]);
      res_local = ray_trace_tile
        (&tiles[itile], render_type, view, cam, buf, tile_origin, tile_size);

      if(UNLIKELY(res_local != RES_OK)) {
        ATOMIC_SET(&res, res_local);
      } else {
        /* Flush tiles into the framebuffer memory */
        res = buf->writer
          (buf->ctx, tile_origin, tile_size, SRDR_UBYTE_RGBA,
           darray_char_cdata_get(&tiles[itile].pixels));
      }
    }
  }

exit:
  if(view) S3D(scene_view_ref_put(view));
  return (res_T)res;
error:
  goto exit;
}

