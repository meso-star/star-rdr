/* Copyright (C) |Meso|Star> 2015-2018 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include "srdr.h"
#include "srdr_device.h"
#include "srdr_framebuffer.h"

#include <rsys/mem_allocator.h>

#include <string.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
framebuffer_release(ref_T* ref)
{
  struct srdr_framebuffer* buf = CONTAINER_OF(ref, struct srdr_framebuffer, ref);
  struct srdr_device* dev;
  ASSERT(ref);
  dev = buf->dev;
  MEM_RM(dev->allocator, buf);
  SRDR(device_ref_put(dev));
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
srdr_framebuffer_create(struct srdr_device* dev, struct srdr_framebuffer** buf_out)
{
  struct srdr_framebuffer* buf = NULL;
  res_T res = RES_OK;

  if(!dev || !buf_out) {
    res = RES_BAD_ARG;
    goto error;
  }
  buf = MEM_CALLOC(dev->allocator, 1, sizeof(struct srdr_framebuffer));
  if(!buf) {
    res = RES_MEM_ERR;
    goto error;
  }
  SRDR(device_ref_get(dev));
  buf->dev = dev;
  ref_init(&buf->ref);

exit:
  if(buf_out)
    *buf_out = buf;
  return res;
error:
  if(buf) {
    SRDR(framebuffer_ref_put(buf));
    buf = NULL;
  }
  goto exit;
}

res_T
srdr_framebuffer_ref_get(struct srdr_framebuffer* buf)
{
  if(!buf) return RES_BAD_ARG;
  ref_get(&buf->ref);
  return RES_OK;
}

res_T
srdr_framebuffer_ref_put(struct srdr_framebuffer* buf)
{
  if(!buf) return RES_BAD_ARG;
  ref_put(&buf->ref, framebuffer_release);
  return RES_OK;
}

res_T
srdr_framebuffer_setup
  (struct srdr_framebuffer* buf,
   const size_t size[2],
   const srdr_pixel_write_T writer,
   void* ctx)
{
  if(!buf || !size || !writer)
    return RES_BAD_ARG;

  buf->size[0] = size[0];
  buf->size[1] = size[1];
  buf->writer = writer;
  buf->ctx = ctx;
  return RES_OK;
}

