/* Copyright (C) |Meso|Star> 2015-2018 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include "srdr.h"
#include "srdr_device.h"

#include <rsys/mem_allocator.h>

#include <string.h>

struct srdr_render_buffer {
  struct srdr_image img;
  ref_T ref;
  struct srdr_device* dev;
};

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
render_buffer_release(ref_T* ref)
{
  struct srdr_render_buffer* buf = CONTAINER_OF
    (ref, struct srdr_render_buffer, ref);
  struct srdr_device* dev;
  ASSERT(ref);
  dev = buf->dev;
  if(buf->img.data)
    MEM_RM(dev->allocator, buf->img.data);
  MEM_RM(dev->allocator, buf);
  SRDR(device_ref_put(dev));
}

/*******************************************************************************
 *  Exported functions
 ******************************************************************************/
res_T
srdr_render_buffer_create
  (struct srdr_device* dev,
   struct srdr_render_buffer** buf_out)
{
  struct srdr_render_buffer* buf = NULL;
  res_T res = RES_OK;

  if(!dev || !buf_out) {
    res = RES_BAD_ARG;
    goto error;
  }
  buf = MEM_CALLOC(dev->allocator, 1, sizeof(struct srdr_render_buffer));
  if(!buf) {
    res = RES_MEM_ERR;
    goto error;
  }
  SRDR(device_ref_get(dev));
  buf->dev = dev;
  ref_init(&buf->ref);

exit:
  if(buf_out)
    *buf_out = buf;
  return res;
error:
  if(buf) {
    SRDR(render_buffer_ref_put(buf));
    buf = NULL;
  }
  goto exit;
}

res_T
srdr_render_buffer_ref_get(struct srdr_render_buffer* buf)
{
  if(!buf) return RES_BAD_ARG;
  ref_get(&buf->ref);
  return RES_OK;
}

res_T
srdr_render_buffer_ref_put(struct srdr_render_buffer* buf)
{
  if(!buf) return RES_BAD_ARG;
  ref_put(&buf->ref, render_buffer_release);
  return RES_OK;
}

res_T
srdr_render_buffer_setup
  (struct srdr_render_buffer* buf,
   const size_t size[2],
   const enum srdr_pixel_format format)
{
  size_t pitch;
  void* data;

  if(!buf || !size || !size[0] || !size[1])
    return RES_BAD_ARG;

  pitch = size[0] * SRDR_SIZEOF_PIXEL_FORMAT(format);
  data = MEM_ALLOC_ALIGNED(buf->dev->allocator, pitch * size[1], 16);
  if(!data)
    return RES_MEM_ERR;

  if(buf->img.data) {
    MEM_RM(buf->dev->allocator, buf->img.data);
    buf->img.data = NULL;
    buf->img.pitch = 0;
    buf->img.size[0] = buf->img.size[1] = 0;
  }
  buf->img.data = data;
  buf->img.pitch = pitch;
  buf->img.size[0] = size[0];
  buf->img.size[1] = size[1];
  buf->img.format = format;
  return RES_OK;
}

res_T
srdr_render_buffer_image_get
  (struct srdr_render_buffer* buf, struct srdr_image* img)
{
  if(!buf || !img)
    return RES_BAD_ARG;
  *img = buf->img;
  return RES_OK;
}

res_T
srdr_render_buffer_write
  (void* context,
   const size_t origin[2],
   const size_t size[2],
   const enum srdr_pixel_format format,
   const void* pixels)
{
  struct srdr_render_buffer* rbuf = context;
  const char* src_row = pixels;
  size_t BPP;
  size_t iy;
  size_t tile_pitch, itile_x;

  if(UNLIKELY(!context || !origin || !size || !pixels))
    return RES_BAD_ARG;
  if(UNLIKELY(format != rbuf->img.format || !rbuf->img.data))
    return RES_BAD_ARG;

  if(UNLIKELY((origin[0] + size[0]) > rbuf->img.size[0]))
    return RES_BAD_ARG;
  if(UNLIKELY(origin[1] + size[1] > rbuf->img.size[1]))
    return RES_BAD_ARG;

  BPP = SRDR_SIZEOF_PIXEL_FORMAT(rbuf->img.format);
  tile_pitch = size[0] * BPP;
  itile_x = origin[0] * BPP;

  FOR_EACH(iy, origin[1], origin[1] + size[1]) {
    const size_t itile_row = iy * rbuf->img.pitch + itile_x;
    memcpy(rbuf->img.data + itile_row, src_row, tile_pitch);
    src_row += tile_pitch;
  }
  return RES_OK;
}

