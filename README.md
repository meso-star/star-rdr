# Star Renderer

This code bases draws a virtual scene managed by the
[Star-3D](https://gitlab.com/meso-star/star-3d/) library. It provides the data
structure specific to a renderer like the camera or the framebuffer; the shapes
should be explicitly managed by the client side code through the Star-3D
library. Note that the rendering only compute basic ray-casting; neither
advanced lighting nor material shading is implemented.

## How to build

The Star-Renderer library relies on the [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) package to build. It also depends on
the [RSys](https://gitlab.com/vaplv/rsys/) and the
[Star-3D](https://gitlab.com/meso-star/star-3d/) libraries.

First ensure that CMake is installed on your system. Then install the RCMake
package as well as the RSys and the Star-3D libraries. Finally generate the
project from the `cmake/CMakeLists.txt` file by appending to the
`CMAKE_PREFIX_PATH` variable the `<RCMAKE_INSTALL_DIR>`, `<RSYS_INSTALL_DIR>`
and `<S3D_INSTALL_DIR>` directories, where `<RCMAKE_INSTALL_DIR>`,
`<RSYS_INSTALL_DIR>` and `<S3D_INSTALL_DIR>` are the install directories of the
RCMake package, the RSys and the Star-3D libraries, respectively. The generated
project can be edited, built, tested and installed as any CMake project.

## License

Star-Renderer is Copyright (C) |Meso|Star> 2015-2018 (<contact@meso-star.com>).
It is a free software released under the [OSI](http://opensource.org)-approved
CeCILL license. You are welcome to redistribute it under certain conditions;
refer to the COPYING files for details.

